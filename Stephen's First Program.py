import random 

class card(object):
  def __init__(self, val, suite):
    self.val = val
    self.suite = suite
  
  def printVals(self):
    str = ""
    if self.suite == 0:
      str = "diamonds"
    elif self.suite == 1:
      str = "spades"
    elif self.suite == 2:
      str = "clubs"
    elif self.suite == 3:
      str = "hearts"
    print(self.val, "of", str)

class deck(object):
  def __init__(self):
    self.cards = []
    for suite in range(0, 4):
      for val in range(0, 13):
        newCard = card(val, suite)
        self.cards.append(newCard)
  
  def pickrandomCard(self):
    indextopick = random.randint(0, len(self.cards))#get random integer
    self.cards[indextopick].printVals()

#EXECUTE:
myDeck = deck()
myDeck.pickrandomCard()
